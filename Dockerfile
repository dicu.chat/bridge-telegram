# syntax=docker/dockerfile:experimental

# Defining variables for different dependencies.
ARG VERSION=3.9-slim-buster


# Builder for the wheels
FROM python:${VERSION} as builder
# Installing dependencies for building wheels and/or for containers.
RUN apt-get -y update \
 && apt-get -qqy --no-install-recommends install \
    # Required to build crypto libs. \
    gcc
COPY requirements.txt .
RUN pip install -U pip wheel setuptools \
 && mkdir /wheels \
 && pip wheel -r requirements.txt -w /wheels


FROM python:${VERSION}
WORKDIR /usr/src/app
# Temporarily mount /wheels dir and install wheels from there. This is possible due to the 'experimental' syntax on the line 1.
RUN --mount=type=bind,source=/wheels,target=/wheels,from=builder pip install --no-cache-dir /wheels/*
COPY server server
CMD python3 -m server
