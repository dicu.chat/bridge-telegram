# Telegram Bot token

To create a bot in Telegram, you must first contact the BotFather. BotFather is the one and only bot who can rule them all. It can be used to create new bot accounts as well as manage your existing bots.

You can either search telegram chats for @BotFather or [click here](https://t.me/botfather) to find the botfather.

Please Follow these steps to create a new bot in telegram using botfather:

1. Type / on the chat it will displays all the possible options. Pick the /newbot option.

2. Type a name for your new bot.

3. Choose a username for your bot. It must end in `bot`. Like this, for example: TetrisBot or tetris_bot.

4. After that it will displays a message with the bot token to access the HTTP API, copy this token and paste it inside the env file.

If you already have a bot in Telegram, you can simply ask Botfather for its token by following these steps:

1. Type /mybots. 
2. Choose your botname.
3. A menu of options will display Pick API token option. 

