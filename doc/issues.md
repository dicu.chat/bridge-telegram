# Issues

General issues may occur during development:

### Issue #1: If you couldnt find the vim command:
```js
Command 'vim' not found,
```


**Solution**: 

Pick one of these commands in order to download the vim in your device: 
```bash
sudo apt install vim         # version 2:8.1.2269-1ubuntu5.3, or
sudo apt install vim-tiny    # version 2:8.1.2269-1ubuntu5.3
sudo apt install vim-athena  # version 2:8.1.2269-1ubuntu5.3
sudo apt install vim-gtk3    # version 2:8.1.2269-1ubuntu5.3
sudo apt install vim-nox     # version 2:8.1.2269-1ubuntu5.3
sudo apt install neovim      # version 0.4.3-3

```



### Issue #2: No module named 'apt_pkg', if you have seen this error while you running the command `vim .env`, it means that your apt of python3.9 is somehow broken.

```js
  File "/usr/lib/python3/dist-packages/CommandNotFound/db/db.py", line 5, in <module>
    import apt_pkg
ModuleNotFoundError: No module named 'apt_pkg'
```

**Solution**: 
1. You may reinstall the apt fill by running this command:
```js
sudo apt-get install python3-apt --reinstall
```
2. if its still having the same issue you can run these commands

```bash

cd /usr/lib/python3/dist-packages

sudo ln -s apt_pkg.cpython-{your-version-number}-x86_64-linux-gnu.so apt_pkg.so
```

***Replace {your-version-number} appropriately.***



### Issue #3: if your Terminal doesnt open or respoding due to upgrading the python version to 3.9. 


if you updated your python version inorder to run our project, and then you ran into apt error and teriminal error in your device, well thats beacuse python3-apt checks the highest python version, instead of the current python version in use.

**Solution**: 
so to solve this issue you can set the defulat back to older version:
```bash
sudo update-alternatives --install /usr/bin/python3 python3 /usr/bin/python<Your older version ex:3.6 > 1
sudo update-alternatives --install /usr/bin/python3 python3 /usr/bin/python3.9 2

sudo update-alternatives --config python3 <Then pick the number which indicate the older version>

```


### Issue #4: Invalid token.

if you run the project and this problem occurs
```bash
raise InvalidToken()
telegram.error.InvalidToken: Invalid token
```
it means you didnt fill the bot token either the server token with the correct tokens. Please check this document [here](./doc/telegrambot.md) which explains how to get the bot token.

