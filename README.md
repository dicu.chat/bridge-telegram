<h1 align="center">
  <br>
  <a href="https://www.morphysm.com/"><img src="./assets/morph_logo_rgb.png" alt="Morphysm" ></a>
  <br>
  <h5 align="center"> Morphysm is a community of engineers, designers and researchers
contributing to security, cryptography, cryptocurrency and AI.</h5>
  <br>
</h1>


<h1 align="center">
 <img src="https://img.shields.io/badge/Python-3.9-brightgreen" alt="python badge">

 <img src="https://img.shields.io/badge/docker-20.10-blue" alt="docker badge">

 <img src="https://img.shields.io/badge/version-1.1.8-orange" alt="version badge">
</h1> 


Table of Contents
=================

<!--ts-->
   * [Getting Started](#getting-started)
   * [Prerequisites](#prerequisites)
   * [Installation](#installation)
   * [Development](#development)
   * [Production](#production)
   * [TroubelShoting](#troubelShoting)
   * [Code Owners](#code-owners)
   * [License](#license)
   * [Contact](#contact)
<!--te-->



# 1. Getting Started
This repository connects our chatbot server to the Telegram bot channel, by constructing message bridges from Telegram to the server code and vice versa. Please keep in mind that in order to understand the functionality, you must also run our [server repo](https://gitlab.com/dicu.chat/server) with this repo to see the desired results, otherwise, it will work but with no obvious results.



# 2. Prerequisites
- `docker`  
- `docker-compose`  
- `python3.9`  

# 3. Installation
To download our repo please wite this command
either:
```bash
git@gitlab.com:dicu.chat/bridge-telegram.git
```
or:
```bash
https://gitlab.com/dicu.chat/bridge-telegram.git
```

# 4. Development 

Please follow these steps to help you run our project (Not Dokerized):

1. Create dev python environment for the project:  
    ```bash
    virtualenv -p python3.9 .venv && source .venv/bin/activate
    ```

2. Install dependencies:  
    ```bash
    python -m pip install -U pip wheel setuptools
    python -m pip install -r requirements.txt
    ```


3. Copy the `.env` file:  
    ```bash
    cp .env.example .env
    vim .env
    ```

4. Fille the `.env` file with the missing information of the tokens:
      1. BOT_TOKEN: To obtain the bot token, either create a new bot using the botFather in Telegram and take its token, or if you already have a bot, take its token and fill it inside the env file. If you don't understand these concepts, you can read more [here!](./doc/telegrambot).


      2. SERVER_TOKEN: You can generate a random string and fill the server token in the env file, but keep in mind that this token must also be filled in the server repo please check it out [here!](https://gitlab.com/dicu.chat/server)


      Example:
            ```bash
            BOT_TOKEN=2137977127:AAECpd4GmUx1niHjCVDDThoKhVAdCPvQ37o
            SERVER_TOKEN=Kp4I8kXkuT_very_secret
            ```

5. Run the project:
    ```bash
    python -m server
    ```


# 5. Production 
For production setup look into [examples](https://gitlab.com/dicu.chat/server/-/tree/master/examples/all) directory in our core repo, which contains full dicu.chat stack setup example.  
Otherwise, you can just run:  

- Build & run docker:  
    ```bash
    docker-compose up -d --build
    ``` 

# 6. TroubelShoting

If you have encountered any problems while running the code, please consult [this file](./doc/issues.md), which contains potential solutions for general issues, otherwise, please open a new issue in this repo and label it bug, and we will assist you in resolving it.


# 7. Code Owners

@morphysm/team    :sunglasses:

# 8. License
Our reposiorty is licensed under the terms of the GPLv3 see the license [here!](https://gitlab.com/dicu.chat/bridge-telegram/-/blob/master/LICENSE)

# 9. Contact
If you'd like to know more about  us [Click here!](https://www.morphysm.com/), or You can contact us at "contact@morphysm.com".

