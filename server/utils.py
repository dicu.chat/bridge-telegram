from telethon import TelegramClient, Button, functions, types
from typing import Optional
import aiohttp
import asyncio
import logging


def build_menu(buttons, n_cols, header_buttons=None, footer_buttons=None):
    menu = [buttons[i:i + n_cols] for i in range(0, len(buttons), n_cols)]

    if header_buttons:  menu.insert(0, [header_buttons])
    if footer_buttons:  menu.append([footer_buttons])

    return menu


def build_headers(creds: dict):
    return {
        "Content-Type": "application/json",
        "Authorization": f"Bearer {creds['token']}",
        "Instance-Name": creds["name"],
        "Service-In": "telegram",
    }


def build_payload(user, chat_id: int, text: Optional[str] = None, voice: Optional[dict] = None):
    assert (text is not None or voice is not None), "One of the values ('text' or 'voice') must be provided!"

    return {
        "user": {
            "userId": str(user.id),
            "firstName": user.first_name,
            "lastName": user.last_name,
            "username": user.username,
            "langCode": user.lang_code
        },
        "chat": {
            "chatId": str(chat_id),
        },
        "message": {
            "text": text,
            "voice": voice,
        },
        "context": {
            "wantedExtension": "ogg",
            "wantedCodec": "libopus",
            "expectingVoice": bool(voice),
        }
    }


async def make_setup_request(session: aiohttp.ClientSession, settings):
    headers = {"Authorization": f"Bearer {settings.SERVER_TOKEN}"}
    payload = {"url": settings.INSTANCE_URL}

    # Blocking until connected.
    while True:
        try:
            url = f"{settings.SERVER_URL}/api/setup"
            async with session.post(url, json=payload, headers=headers) as r:
                resp = await r.json()
                if resp.get("status", -1) != 200:
                    logging.info("Erroneus response during setup: %s", resp)
                    return

                logging.info("Authorization with the server succeed.")
                return resp["name"], resp["token"]

        except aiohttp.client_exceptions.ClientConnectorError:
            sleep_time = 15
            logging.error("Requests to the server (url '%s') for authorization failed! Retrying in %s seconds, sleeping until then.", url, sleep_time)
            await asyncio.sleep(sleep_time)


async def make_server_request(session: aiohttp.ClientSession, url: str, payload: dict, headers: dict = None):
    async with session.post(url, json=payload, headers=headers) as r:
        resp = await r.json()
        if resp.get("status", -1) != 200:
            logging.info("Erroneus response: %s", resp)
            return


async def make_telegram_request(client: TelegramClient, data: dict):
    buttons = []
    for block in data["buttons"]:
        if isinstance(block, list):
            buttons.append([Button.text(button["text"], resize=True) for button in block])
        else:
            buttons.append(Button.text(block["text"], resize=True))

    menu = build_menu(buttons, 2) or Button.clear()

    if data["message"]["voice"]:
        logging.info("Voice: %s", data["message"]["voice"]["content"])
        await client.send_file(
            int(data["chat"]["chatId"]),
            data["message"]["voice"]["content"],
            voice_note=True, buttons=menu,
        )

    else:
        await client.send_message(
            int(data["chat"]["chatId"]), data["message"]["text"],
            parse_mode="html", link_preview=False, buttons=menu,
            # Telethon's "send_message" supports any files.
            # TODO: not assume 1 file per msg.
            file=data["files"][0]["content"] if data["files"] else None,
        )


async def make_set_commands_request(client: TelegramClient, commands: dict) -> bool:
    telethon_commands = [
        types.BotCommand(
            command     = c.removeprefix("/"),
            description = d
        ) for c, d in commands.items()
    ]

    success = await client(functions.bots.SetBotCommandsRequest(
        scope=types.BotCommandScopeDefault(), lang_code="en", commands=telethon_commands
    ))
    return success


async def typing_task(client: TelegramClient, event: asyncio.Event, chat_id: str, message_count: int, mode: str):
    for _ in range(message_count):
        async with client.action(int(chat_id), "record-voice" if mode == "recording_voice" else "typing"):
            logging.debug("Sending typing action to '%s'! ...", chat_id)
            await event.wait()
            event.clear()
            logging.debug("Typing action done for '%s' is done!", chat_id)

