from telethon import events
import logging
import base64

from ..utils import build_payload, build_headers, make_server_request


async def init(client, context):
    session = context["session"]
    settings = context["settings"]

    @client.on(events.NewMessage(func=lambda e: e.text))
    async def text_message_handler(event):
        user = await event.get_sender()

        payload = build_payload(user, event.chat_id, text=event.text)
        headers = build_headers(context["creds"])

        await make_server_request(session, f"{settings.SERVER_URL}/api/process_message", payload=payload, headers=headers)

    @client.on(events.NewMessage(func=lambda e: e.voice))
    async def voice_message_handler(event):
        user = await event.get_sender()

        voice_bytes = await event.download_media(file=bytes)
        logging.debug("Downloaded voice file: %s", voice_bytes)

        voice = {
            "mediaType": "voice",
            "contentType": "bytes",
            "content": base64.b64encode(voice_bytes).decode(),
            "extension": "ogg",
            "codec": "libopus",
        }

        payload = build_payload(user, event.chat_id, voice=voice)
        headers = build_headers(context["creds"])

        await make_server_request(session, f"{settings.SERVER_URL}/api/process_message", payload=payload, headers=headers)

