from telethon import TelegramClient
from sanic.response import json
from time import time
import asyncio
import logging

from .utils import build_menu, make_telegram_request, make_set_commands_request, typing_task


def init(app, context):
    client: TelegramClient = context["client"]
    settings = context["settings"]

    @app.middleware("request")
    async def request_validation(request):
        # Using standart authentication method provided by the sanic framework. Token here is a value
        # extracted from the 'Authorization: Token <value>' or 'Authorization: Bearer <value>' headers.
        if not (token := request.token):
            return json({
                "status": 403,
                "message": "Token is missing! You must provide 'Authorization: Token <token>' or 'Authorization: Bearer <token>' header."
            }, status=403)

        if token != settings.SERVER_TOKEN:
            return json({"status": 403, "message": "Token unauthorized (bad token)."}, status=403)

        # Additional validation for the POST body data.
        if request.method == "POST":
            if not request.json or not isinstance(request.json, dict):
                return json({"status": 403, "message": "expected json object"}, status=403)

    @app.route("/", methods=["POST"])
    async def message_handler(request):
        chat_id = request.json["chat"]["chatId"]
        event = context["typing"][chat_id]
        event.set()
        logging.debug("Event for '%s' has been set!", chat_id)

        asyncio.create_task(make_telegram_request(client, request.json))
        return json({"status": 200, "timestamp": time()})

    @app.route("/api/events/actions", methods=["POST"])
    async def actions_handler(request):
        name = request.json.get("name", "")
        if name == "typing" or name == "recording_voice":
            data = request.json.get("data")

            chat_id = data["chatId"]
            message_count = data["messageCount"]

            event = context["typing"][chat_id]
            event.clear()
            logging.debug("Event for '%s' has been cleared and typing task dispatched!", chat_id)

            asyncio.create_task(typing_task(client, event, chat_id, message_count, mode=name))
            return json({"status": 200, "timestamp": time()})

        else:
            return json({"status": 400, "message": f"Unrecognized action: '{name}'!"})

    @app.route("/api/export/commands", methods=["POST"])
    async def commands_handler(request):
        success = await make_set_commands_request(client, request.json)
        return json({"status": 200 if success else 400, "timestamp": time()})

