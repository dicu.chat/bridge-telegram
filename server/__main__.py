from collections import defaultdict
from telethon import TelegramClient
from sanic import Sanic
import asyncio
import aiohttp
import logging

from .utils import make_setup_request
from .settings import settings

from .modules import init as tinit
from .webhook import init as sinit


def main():
    loop = asyncio.get_event_loop()

    async def _main():
        client = TelegramClient(session=settings.SESSION_NAME, api_hash=settings.API_HASH, api_id=settings.API_ID)
        app = Sanic(name="Bridge-Telegram Server")

        global_context = dict(client=client, session=aiohttp.ClientSession(), settings=settings, typing=defaultdict(asyncio.Event))

        name, token = await make_setup_request(global_context["session"], settings)
        global_context["creds"] = {"name": name, "token": token}

        sinit(app, global_context)

        await tinit(client, global_context)
        await client.start(bot_token=settings.BOT_TOKEN)

        server = await app.create_server(host="0.0.0.0", port=settings.INSTANCE_PORT, access_log=False, debug=False, return_asyncio_server=True)
        await server.startup()

        await client.catch_up()

    loop.run_until_complete(_main())
    try:
        loop.run_forever()
    finally:
        loop.stop()


if __name__ == "__main__":
    main()

