import os
import logging
from dotenv import load_dotenv
load_dotenv()

formatter = "%(asctime)s - %(filename)s - %(levelname)s - %(message)s"
date_format = "%d-%b-%y %H:%M:%S"
logging.basicConfig(level=logging.INFO, format=formatter, datefmt=date_format)

# Telegram token
BOT_TOKEN = os.getenv("BOT_TOKEN", "")
# Security tokens
SERVER_TOKEN = os.getenv("SERVER_TOKEN", "")
# Sever url
SERVER_URL = os.getenv("SERVER_URL", "")
# Instance url
INSTANCE_URL = os.getenv("INSTANCE_URL")
INSTANCE_PORT = os.getenv("INSTANCE_PORT", 8080)

SESSION_NAME = "bot"
API_HASH = "a3406de8d171bb422bb6ddf3bbd800e2"
API_ID = 94575

